package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDAO;
import com.dto.Student;


@WebServlet("/UpdateStudent")
public class UpdateStudent extends HttpServlet {
	
	private static final int Id = 0;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int sId = Integer.parseInt(request.getParameter("sId"));
		String sName = request.getParameter("sName");
		String place = request.getParameter("place");
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		Student emp = new Student(sId, sName, place, gender, emailId, password);
		
		StudentDAO empDao = new StudentDAO();
		int result = empDao.updateStudent(emp);
		
		if (result > 0) {
			request.getRequestDispatcher("GetAllStudents").forward(request, response);
		} else {
			request.getRequestDispatcher("HRHomePage.jsp").include(request, response);
			
			out.println("<center>");
			out.println("<h1 style='color:red;'>Unable to Update the Employee Record!!!</h1>");	
			out.println("</center>");
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
