package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDAO;
import com.dto.Student;


@WebServlet("/GetAllStudents")
public class GetAllStudents extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		StudentDAO empDao = new StudentDAO();		
		List<Student> empList = empDao.getAllStudents();
		
		if (empList != null) {
			
			//Storing employees list under request object
			request.setAttribute("empList", empList);
			
			RequestDispatcher rd = request.getRequestDispatcher("GetAllStudentss.jsp");
			rd.forward(request, response);			
		
		} else {	
			
			RequestDispatcher rd = request.getRequestDispatcher("HRHomePage.jsp");
			rd.include(request, response);
			
			out.println("<center>");
			out.println("<h1 style='color:red;'>Unable to Fetch the Students Record(s)!!!</h1>");	
			out.println("</center>");
		}		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
