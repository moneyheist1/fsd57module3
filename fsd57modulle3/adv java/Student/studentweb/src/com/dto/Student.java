package com.dto;

public class Student {
	private int sId;
	private String sName;
	private String place;
	private String gender;
	private String emailId;
	private String password;
	
	public Student() {
		super();
	}

	public Student(int sId, String sName, String place, String gender, String emailId, String password) {
		super();
		this.sId = sId;
		this.sName = sName;
		this.place = place;
		this.gender = gender;
		this.emailId = emailId;
		this.password = password;
	}

	public int getsId() {
		return sId;
	}
	public void setsId(int sId) {
		this.sId =sId;
	}

	public String getsName() {
		return sName;
	}
	public void setsName(String sName) {
		this.sName = sName;
	}

	public String getplace() {
		return place;
	}
	public void setplace(String place) {
		this.place = place;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Student [sId=" + sId + ", sName=" + sName + ", place=" + place + ", gender=" + gender
				+ ", emailId=" + emailId + ", password=" + password + "]";
	}
}
