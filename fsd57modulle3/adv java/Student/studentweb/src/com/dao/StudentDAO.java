package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;
import com.dto.Student;

public class StudentDAO {

	public Student empLogin(String emailId, String password) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String loginQuery = "Select * from student1 where emailId=? and password=?";
		
		
		try {
			pst = con.prepareStatement(loginQuery);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				Student emp = new Student();
				emp.setsId(rs.getInt(1));
				emp.setsName(rs.getString(2));
				emp.setplace(rs.getString(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));
				return emp;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

	public List<Student> getAllStudents() {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;		
		List<Student> empList = null;
		
		String selectQuery = "Select * from student1";
		
		
		try {
			pst = con.prepareStatement(selectQuery);
			rs = pst.executeQuery();
			
			empList = new ArrayList<Student>();
			
			while (rs.next()) {
	Student emp = new Student();
				
				emp.setsId(rs.getInt(1));
				emp.setsName(rs.getString(2));
				emp.setplace(rs.getString(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));
				
				empList.add(emp);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return empList;
	}

	public int registerStudent(Student emp) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		String insertQuery = "insert into student1 " + 
		"(sName, place, gender, emailId, password) values (?, ?, ?, ?, ?)";
		
		try {
			pst = con.prepareStatement(insertQuery);
			
			pst.setString(1, emp.getsName());
			pst.setString(2, emp.getplace());
			pst.setString(3, emp.getGender());
			pst.setString(4, emp.getEmailId());
			pst.setString(5, emp.getPassword());
			
			return pst.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return 0;
	}

	public Student getStudentById(int sId) {	
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String selectQuery = "Select * from student1 where empId=?";
		
		
		try {
			pst = con.prepareStatement(selectQuery);
			pst.setInt(1, sId);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				
				Student emp = new Student();
				
				emp.setsId(rs.getInt(1));
				emp.setsName(rs.getString(2));
				emp.setplace(rs.getString(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));
				
				return emp;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

	public int deleteStudent(int sId) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		String deleteQuery = "delete from student1 where empId=?";
		
		
		try {
			pst = con.prepareStatement(deleteQuery);
			pst.setInt(1, sId);
			
			return pst.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return 0;
	}

	public int updateStudent(Student emp) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		String updateQuery = "update student1 set sName=?, place=?, gender=?, emailId=?, password=? where empId=?";
		
		try {
			pst = con.prepareStatement(updateQuery);
			
			pst.setString(1, emp.getsName());
			pst.setString(2, emp.getplace());
			pst.setString(3, emp.getGender());
			pst.setString(4, emp.getEmailId());
			pst.setString(5, emp.getPassword());
			pst.setInt(6, emp.getsId());
			
			return pst.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return 0;
	}
	
}








