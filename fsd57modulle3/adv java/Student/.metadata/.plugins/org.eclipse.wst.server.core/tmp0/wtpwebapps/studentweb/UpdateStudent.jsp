<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>UpdateEmployee</title>
</head>
<body>

	<jsp:include page="HRHomePage.jsp" />
	
	
	<form action="UpdateStudent" method="post">

	<table align="center">
		<tr>
			<td>EmpId</td>
			<td><input type="text" name="sId" value="${emp.sId}" readonly/></td>
		</tr>
		<tr>
			<td>EmpName</td>
			<td><input type="text" name="sName" value="${emp.sName}" /></td>
		</tr>
		<tr>
			<td>Salary</td>
			<td><input type="text" name="place" value="${emp.place}" /></td>
		</tr>
		<tr>
			<td>Gender</td>
			<td>
				<select name="gender">
					<option value="${emp.gender}" selected>${emp.gender}</option>
					<option value="Male">Male</option>
					<option value="Female">Female</option>
					<option value="Others">Others</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>EmailId</td>
			<td><input type="text" name="emailId" value="${emp.emailId}" /></td>
		</tr>
		<tr>
			<td>Password</td>
			<td><input type="password" name="password" value="${emp.password}" readonly/></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<button>Update Student</button>
			</td>
		</tr>
	</table>
</form>	
</body>
</html>