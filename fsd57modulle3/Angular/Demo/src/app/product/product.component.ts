import { Component } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent {
  products = [
    { id: 1, name: 'MRF Bat',   description: '',        cost: 200.00,imageUrl: 'https://th.bing.com/th/id/OIP.twvkHstRmDE0lMvLSzaH1gHaLN?rs=1&pid=ImgDetMain' },
    { id: 2, name: 'Neck chain',  description: 'Simple neck chain',                  cost: 900.99 ,imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRgI1rvegDOch2sM8ulvdjuCBW4L6EQhUVnjw&usqp=CAU+2'},
    { id: 3, name: 'Braclet',     description: 'Minimal Braclet',                    cost: 250.49 ,imageUrl: 'https://www.soni.fashion/cdn/shop/files/heart-shape-diamond-lovely-design-gold-plated-bracelet-ladies-style-a289-soni-fashion-928.jpg?v=1702295194+3'},
    { id: 4, name: 'Anklet',      description: 'Evil Eye Anklet',                    cost: 150.99,imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQK7EG1KUop0qqzzoFss6hiPKFWgih_uKxWWk_YqFkjHduNxnigriZTAx3UFjrqIIBFLkk&usqp=CAU+4' },
    { id: 5, name: 'Rings',       description: 'Finger Rings ',                      cost: 450.99,imageUrl: 'https://carltonlondon.co.in/cdn/shop/files/b592331r2_1.jpg?v=1689923093&width=1500+5' },
    { id: 6, name: 'Nose pin',    description: 'Traditional and stylish Nose pin',   cost: 200.99 ,imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRsfSFVmscMYYjsk5mymMiKqhDc8eW0N7Kqag&usqp=CAU+6'},
  ];

  addToCart(product: any): void {
    console.log('Product added to cart:', product);
  }
}