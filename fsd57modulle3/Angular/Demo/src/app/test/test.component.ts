import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {
  id:number;
  name:string;
  avg:number;
  address:any;
  hobbies:any;
  constructor(){
    // alert("constructor invoked...");
    this.id=101;
    this.name='ranjith';
    this.avg=50.45;

    this.address={
      streetNo:102,
      city:'hyderabad',
      state:'telangana'
    };
    this.hobbies=['sleeping','drinking','roming']
  }
ngOnInit() {
  alert("ngOnInit invoked..");
  
}
}
