package jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
public class Deleting {

	public static void main(String[] args) {
		
		Connection con = null;
		Statement stmt = null;
		

		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "Delete from employee where empid = 105"; ;

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");

			stmt = con.createStatement();
			int rowAffected = stmt.executeUpdate(query);

			if (rowAffected>0){
				System.out.println("Data deleted successfully");
			}else{
				System.out.println("Failed to delete data");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

}
