package jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
public class updating {

	public static void main(String[] args) {
		
		Connection con = null;
		Statement stmt = null;
		

		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "Update employee set empName = 'bunny' where empid=106";

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");

			stmt = con.createStatement();
			int rowAffected = stmt.executeUpdate(query);

			if (rowAffected>0){
				System.out.println("Data updated successfully");
			}else{
				System.out.println("Failed to update data");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

}


