package day3;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;


import com.db.dbConnection;

public class Deleting {

	public static void main(String[] args) {
		Connection con = dbConnection.getConnection();
		PreparedStatement pst = null;
		
		System.out.println("Enter Employee Id");
		Scanner scan = new Scanner(System.in);
		int empId = scan.nextInt();
		System.out.println();
		String updateQuery = "Delete from employee where empId = ?";
		
		try{
			pst=con.prepareStatement(updateQuery);
			pst.setInt(1, empId);
			int result = pst.executeUpdate();
			
			if(result>0){
				System.out.println("Record Deleted of "+empId);
				
			}else{
				System.out.println("Failed to delete th record of  "+empId);
				
			}
		}catch (SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try{
					pst.close();
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}
	}

}


