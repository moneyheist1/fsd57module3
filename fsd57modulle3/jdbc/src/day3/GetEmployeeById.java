package day3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.dbConnection;

public class GetEmployeeById {
    public static void main(String[] args) {
        Connection con = dbConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        System.out.println("Enter Employee Id to get record:");
        Scanner scan = new Scanner(System.in);
        int empId = scan.nextInt();
        System.out.println();

        String selectQuery = "select * from employee where empId = ?";

        try {
            pst = con.prepareStatement(selectQuery);
            pst.setInt(1, empId);
            rs = pst.executeQuery();

            if (rs.next()) {
                int employeeId = rs.getInt("empId");
                String empName = rs.getString("empName");
                double salary = rs.getDouble("salary");
                String gender = rs.getString("gender");
                String emailId = rs.getString("emailId");
                String password = rs.getString("password");

                System.out.println("Employee ID: " + employeeId);
                System.out.println("Employee Name: " + empName);
                System.out.println("Salary: " + salary);
                System.out.println("Gender: " + gender);
                System.out.println("Email ID: " + emailId);
                System.out.println("Password: " + password);
            } else {
                System.out.println("No record found for the given Employee ID.");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    pst.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
