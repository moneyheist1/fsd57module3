package day3;

	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.util.Scanner;

	import com.db.dbConnection;

	public class GetEmployeeByName {
	    public static void main(String[] args) {
	        Connection con = dbConnection.getConnection();
	        PreparedStatement pst = null;
	        ResultSet rs = null;

	        System.out.println("Enter Employee Name to get record:");
	        Scanner scan = new Scanner(System.in);
	        String empName = scan.nextLine();
	        System.out.println();

	        String selectQuery = "select * from employee where empName = ?";

	        try {
	            pst = con.prepareStatement(selectQuery);
	            pst.setString(1, empName);
	            rs = pst.executeQuery();

	            if (rs.next()) {
	                int employeeId = rs.getInt("empId");
	                String employeeName = rs.getString("empName");
	                double salary = rs.getDouble("salary");
	                String gender = rs.getString("gender");
	                String emailId = rs.getString("emailId");
	                String password = rs.getString("password");

	                System.out.println("Employee ID: " + employeeId);
	                System.out.println("Employee Name: " + employeeName);
	                System.out.println("Salary: " + salary);
	                System.out.println("Gender: " + gender);
	                System.out.println("Email ID: " + emailId);
	                System.out.println("Password: " + password);
	            } else {
	                System.out.println("No record found for the given Employee Name.");
	            }

	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {
	            if (con != null) {
	                try {
	                    if (rs != null) {
	                        rs.close();
	                    }
	                    pst.close();
	                    con.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	    }
	}
