

package day2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

//Update Employee Salary
public class Update4 {
	public static void main(String[] args) {

		Connection con = null;
		Statement stmt = null;

		System.out.println("Enter Employee Id and New Salary");
		Scanner scan = new Scanner(System.in);
		int empId = scan.nextInt();;
		double salary = scan.nextDouble();
		System.out.println();
		
		String url = "jdbc:mysql://localhost:3306/fsd57";
		
		String updateQuery = "update employee set salary = " + salary + " where empId = " + empId;
		
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");

			stmt = con.createStatement();
			int result = stmt.executeUpdate(updateQuery);

			if (result > 0) {
				System.out.println("Employee Record Updated");
				
                   String qry = "Select* from employee";
				
				ResultSet resultSet = stmt.executeQuery(qry);
				System.out.println("\nEmployee Records:");
				
				while (resultSet.next()) {
					int id = resultSet.getInt("empId");
					String name = resultSet.getString("empName");
					double empSalary = resultSet.getDouble("salary");
					String empGender = resultSet.getString("gender");
					String empEmail = resultSet.getString("emailId");
					String empPassword = resultSet.getString("password");

					System.out.println("ID: " + id + ", Name: " + name + ", Salary: " + empSalary + ", Gender: "
							+ empGender + ", Email: " + empEmail + ", Password: " + empPassword);
				}
				} else {
				System.out.println("Failed to Update the Employee Record!!!");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
